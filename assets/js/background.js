window.onload = (event) => {
	let footer = document.querySelector("footer");
	let background_divs = document.getElementsByClassName('background');
	let background_array = [...background_divs];
	let pageSize = 0;
	for (let i = 0; i < background_array.length; i++) {
		if (pageSize > footer.offsetTop) {
			do {
				background_divs[i].parentNode.removeChild(background_divs[i]);
			} while (background_divs[i]);
			return;
		}
		else {
			if (pageSize+background_divs[i].height > footer.offsetTop) {
				let lastImageHeight = Math.abs(footer.offsetTop - pageSize);
				background_divs[i].height = lastImageHeight+1;
			}
		pageSize += background_divs[i].height;
		}
	}
}