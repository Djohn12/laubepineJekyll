function toggleMenu() {
	let burger = document.getElementsByClassName('burger');
	burger[0].addEventListener('click', function (e) {
		let menu = document.getElementsByClassName('nav__menu');
		if (menu[0].style.display != 'block') {
			menu[0].style.display = 'block';
			burger[0].setAttribute("aria-expanded", "true");
		}
		else {
			menu[0].style.display = 'none';
			burger[0].setAttribute("aria-expanded", "false");
		}
	});
}
toggleMenu();