# Code source de www.laubepine.net

## v2 : essai de l'implémentation de Jekyll pour faciliter l'ajout et la modification de contenu


# **A destination des administrateurs du site**
*lire jusqu'au bout si vous n'avez pas l'habitude de modifier le site*

## Explications de l'arborescence du projet :

### le dossier \_data comporte deux fichiers

* navigation.yml == les liens dans le menu du site
* vifNavigation.yml == les liens dans le sous-menu "vif" du site



### le dossier \_includes contient

* footer.html == le footer du site
* head.html == l'entête commune à toutes les pages html du site
* modal.html == le diaporama présent sur certaines pages du site (un clic sur une photo fait apparaître un diaporama)
* navigation.html == le menu du site (ce fichier va chercher les fichiers présents dans le dossier \_data pour générer le menu)



### le dossier \_layouts comprends deux fichiers de template

* onlyText.html qui sert pour les pages qui n'ont pas d'images (donc pas de diaporama)
* withImages.html qui sert pour les pages qui ont besoin du diaporama



### le dossier \_sass contient les feuilles de style du site

* \_base.scss == la typo, le style des liens dans les pages, le style des div qui servent de background, la réinitialisation des marges et des titres (pas en gras)
* \_footer.scss == tout le style concercant le footer
*	\_nav.scss == tout le style concernant le menu
* \_slide.scss == tout le style concernant le diaporama
* \_layout.scss == tout le reste et notamment le positionnement de tous les éléments dans les pages et la taille des images



### le dossier \_assets avec deux sous dossiers

* images == l'endroit dans lequel on stocke toutes les images du site
* js == le dossier comportant les fichiers javascript pour le menu, le diaporama (ou modal) et la gestion des images en background



### A la racine

#### les fichiers HTML

un fichier == une page du site

*Exemples:*
* *laps.html contient tout le contenu de la page laps*
* *actions.html correspond à la page action*


**Note**

Une seule exception: le fichier index.html correspond au contenu de la page d'accueil du site



#### les autres fichiers

* README.md == c'est ce que vous êtes en train de lire
* \_config.yml == fichier de configuration du site **(ne pas modifier)**
* package.json == sert à gérer les dépendances du projets (principalement pour l'automatisation) **(ne pas modifier)**
* gulpfile.js == fichier javascript dans lequel on détermine les commandes et processus liés à l'automatisation de certaines tâches **(ne pas modifier)**
* .gitignore == sert à ignorer des dossiers ou fichiers lors du versionnage du code **(ne pas modifier)**



### Le dossier \_site correspond à ce qu'OVH vient chercher pour le mettre à l'adresse laubepine.net.

**Note**
Il ne sert à rien d'aller y faire quoi que ce soit puisque toutes les modifications et les ajouts se feront ailleurs.
Toute modification faite directement dans ce dossier sera supprimée au lancement de n'importe quelle tâche automatique.

Plus d'informations pour modifier/ajouter du contenu plus bas.




***
## Prérequis à toute modification

Pour pouvoir utiliser l'automatisation et modifier le site correctement, il faut que vous ayez node et npm installés sur votre machine. Suivez les consignes

* [Pour Ubuntu](https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/)
* [Pour MacOS](https://blog.teamtreehouse.com/install-node-js-npm-mac)
* [Pour Windows](https://blog.teamtreehouse.com/install-node-js-npm-windows) 

Une fois fait, ouvrez un terminal à la racine du site et lancez la commande `npm install`

Toutes les dépendances nécessaires s'installeront d'elles-même et vous pourrez exécuter les commandes dont on parlera un peu plus bas

***



## Modifications

### Modification de contenu

Le contenu écrit et les images seront toujours situés dans le fichier .html avec le nom qui correspond à la page.
Pour modifier une page il suffit donc de modifier le fichier .html du même nom.



### Modification de style

Toutes les modifications de style vont se faire dans le dossier \_sass. Pour une meilleure lisibilité, il est préférable d'ajouter les modifications dans le fichier adéquat.

*Exemples:*
* *pour modifier le style du menu, aller dans \_nav.scss*
* *pour modifier le footer, aller dans \_footer.scss*



### Modification des images en background

Les images sont placées directement dans les templates (dans le dossier \_layouts).
Au chargement des pages dans un navigateur, du javascript s'occupe de limiter le nombre d'images affichées selon la taille du contenu de la page.

**Ne pas oublier d'ajouter un attribut "alt" dans chaque image**, on peut le laisser vide si l'image n'a qu'un intérêt visuel et ne sert pas le propos contenu dans le reste de la page.

*Exemple:*
*Dans les galeries de la page laps, les images ont toutes un attribut alt="" puisqu'elles ne servent aucun propos particulier*



## Ajouts

### Ajouter une page

* créez un document "nomdelapage.html" au même niveau que les autres fichiers html (racine du site)
* respectez le même format que les autres fichiers html, ils commencent tous par :

` ---`

`layout: nomdulayout (texte ou avec images)`

`title: titredelapage`

`---`

* ajoutez le contenu que vous voulez en dessous



## Voir les modifications/ajouts en temps réel

* Ouvrez un terminal à la racine du site et lancez la commande `gulp`


* Votre navigateur va ouvrir une page à l'adresse *localhost:4000* et vous montrer le site (si ce n'est pas le cas, faites-le à la main ^^ )
* Sitôt une modification faite **et sauvegardée**, le navigateur va rafraîchir la page et vous verrez les changements

**Notes**

Vous pouvez arrêter ce processus à tout moment avec le raccourci Ctrl + C dans le terminal
Vous pouvez relancer la commande à tout moment si vous l'avez arrêtée



## Sauvegarder les modifications ou ajouts pour le site en production


* Ouvrez un terminal à la racine du site
* Lancez d'abord
* Lancez la commande `gulp build`


Vos modifications seront enregistrée dans le dossier \_site (qui correspond à la version en ligne du site)

Il ne vous reste plus qu'à renvoyer tout le dossier sur le serveur OVH.



## Important

**OVH doit toujours aller chercher le contenu du dossier \_site pour mettre en ligne le site à l'adresse laubepine.net**

**Tout le reste n'est là que pour faciliter le développement, la lisibilité et le maintient du code mais il est important de mettre à jour l'ensemble du dossier sur le serveur, peu importe la modification apportée.**

**le contenu du dossier \_site doit toujours être le reflet du reste du dossier laubépine**