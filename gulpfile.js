const gulp = require('gulp');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const prefixer = require('gulp-autoprefixer');
const log = require('fancy-log');
const browserSync = require('browser-sync');
const clean = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');

const siteRoot = '_site';


const htmlFiles = '*.html';
const layouts = '_layouts/*.html';
const includes = '_includes/*.html';
const dataFiles = '_data/*.yml';

const jsFiles = 'assets/js/*.js';
const jsDest = siteRoot + '/assets/js/';

gulp.task('minifyJs', () => {
	return gulp.src(jsFiles)
		.pipe(concat('all.js'))
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(uglify())
		.pipe(gulp.dest(jsDest))
});

const scssFiles = '_sass/*.scss';
const cssDest = siteRoot + '/assets/css/';

gulp.task('minifyCss', () => {
	return gulp.src([
		// 'assets/css/styles.scss',
		scssFiles,
	])
	.pipe(concat('styles.css'))
	.pipe(prefixer({
		cascade: false,
	}))
	.pipe(clean())
	.pipe(gulp.dest(cssDest))
});

const images = 'assets/images/*.+(png|PNG|jpeg|JPEG|jpg|JPG|svg|SVG)';
const lapsImages = 'assets/images/laps/*.+(png|PNG|jpeg|JPEG|jpg|JPG|svg|SVG)';

gulp.task('minImages', () => {
	return gulp.src(images)
		.pipe(imagemin({verbose: true}))
		.pipe(gulp.dest('_site/assets/images'))
});

gulp.task('minLaps', () => {
	return gulp.src(lapsImages)
		.pipe(imagemin({verbose: true}))
		.pipe(gulp.dest('_site/assets/images/laps'))
})

gulp.task('minifyImages', gulp.series('minImages', 'minLaps'));

gulp.task('jekyll-build', () => {
	browserSync.notify('running jekyll build');
	return require('child_process').spawn('jekyll', ['build'])
});


gulp.task('serve', () => {
	browserSync.init({
		files: [siteRoot + '/**'],
		port: 4000,
		server: {
			baseDir: siteRoot
		}
	});
	gulp.watch([htmlFiles, layouts, includes, dataFiles, jsFiles, scssFiles], gulp.series('jekyll-build', 'minifyJs', 'minifyCss'));
});



gulp.task('default', gulp.series('jekyll-build', 'minifyCss', 'minifyJs', 'serve'));

gulp.task('build', gulp.series('jekyll-build', 'minifyCss', 'minifyJs', 'minifyImages'));